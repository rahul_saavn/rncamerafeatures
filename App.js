import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  StatusBar,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import RootNavigation from './app/navigations/navigator';
import { color } from 'react-native-reanimated';


const App = () => {
    return (
      <SafeAreaView style={{flex : 1}}>
        <StatusBar  animated={true} backgroundColor="#D4285F"/>
        <RootNavigation/>
      </SafeAreaView>
    );
};


export default App;
