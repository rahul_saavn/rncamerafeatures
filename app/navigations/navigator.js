import React from 'react';
import {
    Button,
  SafeAreaView,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, HeaderTitle } from '@react-navigation/stack';
import Home from '../screens/home';
import Details from '../screens/details';
import Feeds from '../screens/feeds';
import { Login } from '../auth/login';

const Stack =  createStackNavigator()

const RootNavigation = () => {
    return (
       <NavigationContainer>
         <Stack.Navigator headerMode = 'none'>
           <Stack.Screen name="Login" component={Login} />
           <Stack.Screen name="Home" component={Home}/>
           <Stack.Screen name="Feeds" component={Feeds}/>
           <Stack.Screen name="Details" component={Details} />
        </Stack.Navigator>
       </NavigationContainer>
    );
};


export default RootNavigation;
