import React, { Component } from 'react';
import { View,Text } from 'react-native';

export const Header = props => {
    const {title} = props
    return(
        <View style={{height :'7%',backgroundColor : '#DE0E71',width : '100%',alignContent : 'center',padding : '1%',justifyContent : 'center'}}>
           <Text style={{fontWeight : 'bold',color : 'white',fontSize : 20,paddingLeft : '5%'}}>{title}</Text>
        </View>
    )
}