import  React,{Fragment, useState} from 'react';
import { View,Button, Text,StyleSheet,TouchableOpacity,Image, ScrollView, ToastAndroid } from 'react-native';
import images from '../assets';
import Camera from '../components/RNcamera/camera';
import { Header } from '../shared/header';

export default function Home(props) {
 const {navigation} = props
 const [isCameraVisible,setCameraVisibility] = useState(false)
 const [actionType,setAction] = useState('')
 const [barcodes,setBarcodes] = useState([])
 const [faces,setFace] = useState([])
 const [textString,setText] = useState([])
 const [userUri,setUri] = useState('')

 console.log(navigation)

 const takeAction = action => {
    setCameraVisibility(true)
    setAction(action)
 }

 const takePhoto = (uri) => {
  setUri(uri)
  setCameraVisibility(false)
 }

 const takeScannedBarcode = (barcodes) => {
  setBarcodes(barcodes)
  //alert(JSON.stringify(barcodes))
  setCameraVisibility(false)
 }

 const getFaceDetected = (faces) => {
  setFace(faces)
  //alert(JSON.stringify(face))
  setCameraVisibility(false)
  ToastAndroid.show("Face is detected", ToastAndroid.SHORT);

 }

 const getText = (text) => {
  setText(text)
  setCameraVisibility(false)
 }

 const renderTextBlocks = () => {
   debugger
   return(
   <View>
    {textString.map(({bounds,value}) => {
      return(
        <View key = {Math.random().toString()} style = {{backgroundColor : '#DE0E71',padding : '2%',margin : '1%'}}>
          <Text style={{fontWeight: '600',color  :'white'}}>{value}</Text>
      </View>
      )
    })}
  </View>
   )
 }

 const renderBarcodes = () => {
  debugger
  return(
  <View>
   {barcodes.map(({bounds,data,type}) => {
     return(
       <View key = {Math.random().toString()} style = {{backgroundColor : '#DE0E71',padding : '2%',margin : '1%'}}>
         <Text style={{fontWeight: '600',color  :'white'}}>{`${data} ${type}`}</Text>
     </View>
     )
   })}
 </View>
  )
}



 const renderCamera = () => {
   return(
     <Camera
       action = {actionType}
       isVisible = {isCameraVisible}
       setVisibility = {setCameraVisibility}
       getFaceDetected = {getFaceDetected}
       takeScannedBarcode = {takeScannedBarcode}
       takePhoto = {takePhoto}
       getText = {getText}
     />
   )
 }

 

  return (
    <View style={{ flex: 1,backgroundColor : 'white' }}>
      <Header title = 'Home'/>
      {userUri ? <Image
        source = {{uri : userUri}}
        resizeMode = 'contain'
        style={{alignSelf : 'center',paddingVertical : '5%',height : 100,width : 180,marginTop : 10,}}
      /> : <Image
      source = {images.user}
      resizeMode = 'contain'
      resizeMethod = 'auto'
      style={{alignSelf : 'center',paddingVertical : '10%',height : '10%',width : '30%',marginTop : 10}}
    /> }
    <ScrollView>
      <TouchableOpacity
          style={styles.fingerprint}
          onPress = {() => takeAction('selfee')}
        >
          <Text style={{fontWeight : 'bold',color : 'white'}}>Take Selfee</Text>
        </TouchableOpacity>
        
        <TouchableOpacity
          style={styles.fingerprint}
          onPress = {() => takeAction('detect_face')}
        >
          <Text style={{fontWeight : 'bold',color : 'white'}}>Detect Face</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.fingerprint}
          onPress = {() => takeAction('text_recog')}
        >
          <Text style={{fontWeight : 'bold',color : 'white'}}>Recognize Text</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.fingerprint}
          onPress = {() => takeAction('barcode')}
        >
          <Text style={{fontWeight : 'bold',color : 'white'}}>Scan barcode</Text>
        </TouchableOpacity>
     
        {isCameraVisible && renderCamera()}
        
        {textString.length > 0 && 
         <Fragment> 
           <Text style = {{fontWeight : 'bold',marginLeft : '1%'}}>Recognized Text Pattern</Text>
         {renderTextBlocks()}
        </Fragment> }
        {barcodes.length > 0 && 
         <Fragment> 
           <Text style = {{fontWeight : 'bold',marginLeft : '1%'}}>Scanned Barcode</Text>
         {renderBarcodes()}
        </Fragment> }
        </ScrollView>
    </View>
  );
}

const styles =  StyleSheet.create({
  fingerprint: {
    padding: 15,
    marginVertical: 10,
    backgroundColor : '#DE0E71',
    width : '50%',
    alignItems : 'center',
    alignSelf : 'center'
  
  },
  text: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#F00',
    justifyContent: 'center',
  },
  textBlock: {
    color: '#F00',
    position: 'absolute',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  facesContainer: {
  //  position: 'absolute',
  //  bottom: 0,
  //  right: 0,
  //  left: 0,
  //  top: 0,
  },
  face: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#FFD700',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  landmark: {
    width: 2,
    height: 2,
    position: 'absolute',
    backgroundColor: 'red',
  },
})