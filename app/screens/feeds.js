import React from 'react';
import {
    Button,
  SafeAreaView,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../screens/home';
import Details from '../screens/details';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Feed from './feed';
import Messages from './Messages';


const Tab =  createBottomTabNavigator()

const Feeds = () => {
    return (
        <Tab.Navigator>
         <Tab.Screen name="Feed" component={Feed} options={{ tabBarBadge: 5 }}/>
         <Tab.Screen name="Messages" component={Messages} />
       </Tab.Navigator>
    );
};


export default Feeds;
