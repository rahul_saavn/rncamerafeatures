import React, { PureComponent } from 'react';
import { Image, Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { RNCamera } from 'react-native-camera';
import images from '../../assets';


export default Camera = props => {
    const {navigation,isVisible,setVisibility,takePhoto,action,takeScannedBarcode,getText,getFaceDetected} = props

    let camera = null

    const onPhotoSnap = async() => {
        if (camera) {
            const options = { quality: 0.5, base64: true };
            const data = await camera.takePictureAsync(options);
           // console.log(data.uri);
            takePhoto(data.uri)
          }
    }

    return(
        <Modal 
          animated
          visible = {isVisible}
          animationType = 'slide'
          style={styles.container}
          onRequestClose = {() => setVisibility(false)}
          >
          <View style={{ alignSelf : 'flex-start',padding : '2%',backgroundColor :'#DE0E71',width : '100%',alignItems : 'flex-start' }}>
          <TouchableOpacity onPress={() => setVisibility(false)} style={styles.capture}>
            <Text style={{ fontSize: 14,color : 'white',alignSelf : 'flex-start' }}> Back </Text>
          </TouchableOpacity>
         </View> 
         <RNCamera
          ref={ref => {
            camera = ref;
          }}
          style={styles.preview}
          type={(action == 'selfee' || action == 'detect_face') ? RNCamera.Constants.Type.front : RNCamera.Constants.Type.back }
          flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
           onFacesDetected={({faces}) => action == 'detect_face' && faces.length > 0 && getFaceDetected(faces)}
           onTextRecognized={({textBlocks}) => action == 'text_recog' && textBlocks.length > 0 &&  getText(textBlocks)}
          onGoogleVisionBarcodesDetected={({ barcodes }) => {
            console.log("barcodes",barcodes);
            barcodes.length > 0 && action == 'barcode' && takeScannedBarcode(barcodes)
          }}
          
        />
        {action == 'selfee' && <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity onPress={onPhotoSnap} style={[styles.capture,{backgroundColor : 'white'}]}>
            {/* <Text style={{ fontSize: 14,color : 'white',padding : '2%' }}> CLICK </Text> */}
            <Image
              source = {images.camera}
              resizeMode = 'contain'
              style = {{height : 50,width : 50}}
            />
          </TouchableOpacity>
        </View>}
      </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
     // backgroundColor: '',

    },
    preview: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    capture: {
      flex: 0,
      backgroundColor: '#DE0E71',
      borderRadius: 5,
      padding: '1%',
      paddingHorizontal: 20,
     // alignSelf: 'center',
      margin: 5,
    },
  });
  