import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import FingerprintScanner from 'react-native-fingerprint-scanner';

export const Login = props => {

    const {navigation} = props

    const [isSensorExist, setSensorExist] = useState(false)

    useEffect(() => {
        detectIsSensorAvailable()
    },[])

    const detectIsSensorAvailable = async() => {
       let authType = await FingerprintScanner.isSensorAvailable()
       if(authType == 'Biometrics') loginWithFinger()
    }

    const loginWithFinger = async() => {
       try{
            let authSuccess = await FingerprintScanner.authenticate({ description:  'Log in with Fingerprint'})
            if(authSuccess) navigation.navigate('Home')
        }
       catch(error){
        alert('Fingerprint Authentication', error.message);
       }
       
    }

    return(
        <View style={styles.container}>
         <Text style={styles.heading}>
          Please Login
         </Text>
         
      </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
       // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DE0E71'
      },
      heading: {
        color: '#ffffff',
        fontSize: 22,
        marginTop: 50,
        marginBottom: 5,
        fontWeight : 'bold'
      },
      subheading: {
        color: '#ffffff',
        fontSize: 12,
        textAlign: 'center',
        marginTop: 5,
        marginBottom: 30,
      },
      fingerprint: {
        padding: 15,
        marginVertical: 10,
        backgroundColor : 'white',
        width : '50%',
        alignItems : 'center'
      
      },
})